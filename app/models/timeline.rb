class Timeline
  def initialize(user, scope = Shout)
    @users = user
    @scope = scope
  end

  def shouts
    scope
      .where(user_id: users)
      .order(created_at: :desc)
  end

  def to_partial_path
    'timelines/timeline'
  end

  private

  attr_reader  :users, :scope
end
